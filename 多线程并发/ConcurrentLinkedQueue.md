### J.U.C之Java并发容器：ConcurrentLinkedQueue
- ConcurrentLinkedQueue一个基于链接节点的无界线程安全队列。此队列按照 FIFO（先进先出）原则对元素进行排序 采用“wait-free”算法（即CAS算法）来实现的。队列的头部 是队列中时间最长的元素。队列的尾部 是队列中时间最短的元素。
- 新的元素插入到队列的尾部，队列获取操作从队列头部获得元素。当多个线程共享访问一个公共 collection 时，ConcurrentLinkedQueue 是一个恰当的选择。此队列不允许使用 null 元素。

### CoucurrentLinkedQueue规定了如下几个不变性：
- 在入队的最后一个元素的next为null
- 队列中所有未删除的节点的item都不能为null且都能从head节点遍历到
- 对于要删除的节点，不是直接将其设置为null，而是先将其item域设置为null（迭代器会跳过item为null的节点）
- 允许head和tail更新滞后。这是什么意思呢？意思就说是head、tail不总是指向第一个元素和最后一个元素

 **head的不变性和可变性：** 

```
#不变性
所有未删除的节点都可以通过head节点遍历到
head不能为null
head节点的next不能指向自身

#可变性
head的item可能为null，也可能不为null
允许tail滞后head，也就是说调用succc()方法，从head不可达tail
```
 **tail的不变性和可变性** 

```
#不变性
tail不能为null

#可变性
tail的item可能为null，也可能不为null
tail节点的next域可以指向自身
允许tail滞后head，也就是说调用succc()方法，从head不可达tail
```

###  方法

```
boolean	add(E e) #将指定元素插入此队列的尾部。
boolean	contains(Object o) #如果此队列包含指定元素，则返回 true。
boolean	isEmpty() #如果此队列不包含任何元素，则返回 true。
Iterator<E> iterator() #返回在此队列元素上以恰当顺序进行迭代的迭代器。
boolean	offer(E e) # 将指定元素插入此队列的尾部。
E peek() #获取但不移除此队列的头；如果此队列为空，则返回 null。
E poll() #获取并移除此队列的头，如果此队列为空，则返回 null。
boolean	remove(Object o) #从队列中移除指定元素的单个实例（如果存在）。
int	size() #返回此队列中的元素数量。
Object[]	toArray() #返回以恰当顺序包含此队列所有元素的数组。
<T> T[]	toArray(T[] a) #返回以恰当顺序包含此队列所有元素的数组；返回数组的运行时类型是指定数组的运行时类型。
```

### 代码实例

```
public class CoucurrentLinkedQueueTest {

	/**
	 * 10000个人去饭店吃饭，10张桌子供饭，分别比较size() 和 isEmpty() 的耗时
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		ConcurrentLinkedQueue<String> queue=new ConcurrentLinkedQueue<>();
		 int peopleNum = 10000;//吃饭人数
		CountDownLatch count = new CountDownLatch(10);//计数器
		ExecutorService exec=Executors.newCachedThreadPool();
		 //将吃饭人数放入队列（吃饭的人进行排队）
        for(int i=1;i<=peopleNum;i++){
            queue.offer("客人_" + i);
        }
        
        //执行10个线程从队列取出元素（10个桌子开始供饭）
        System.out.println("-----------------------------------开饭了-----------------------------------");
        long start = System.currentTimeMillis();
        for (int i = 1; i <= 10; i++) {
        	final int task=i;
        	exec.execute(new Runnable() {
				@Override
				public void run() {
				//size() 和 isEmpty() 都可以判断队列中是否还有没有元素。但是size()是遍历队列去判断，isEmpty()不用遍历，效率更高
//					 while (queue.size() > 0){
					 while (!queue.isEmpty()){
			                //从队列取出一个元素 排队的人少一个
			                System.out.println("【" +queue.poll() + "】----已吃完...， 饭桌编号：" + task);
			            }
			            count.countDown();//计数器-1
					
				}
			});
		}
        
        //计数器等待，直到队列为空（所有人吃完）
        count.await();
        long time = System.currentTimeMillis() - start;
        System.out.println("-----------------------------------所有人已经吃完-----------------------------------");
        System.out.println("共耗时：" + time);
        //停止线程池
        exec.shutdown();
	}
}

```

